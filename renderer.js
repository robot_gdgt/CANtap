// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

// const { safeStringifyJson } = require('builder-util-runtime');
const $ = require('cash-dom');
const Mustache = require('mustache');

const selectId = $('#sendForm > select[name="id"]')[0];
let prev = '';
Object.keys(CAN_IDS).sort().forEach(id => {
  if (prev != id.substring(0, 1)) {
    selectId.appendChild(new Option('', ''));
    prev = id.substring(0, 1);
  }
  selectId.appendChild(new Option(`0x${id} ${CAN_IDS[id].name}`, id));
});

let filterMask = 0x000; // Show all
let filterId = 0x000; // Show all

const templateRaw = $('#templateRaw').text();
// _(templateRaw)
const $tbodyRaw = $('#tableRaw > div.tbody');
const templateDecoded = $('#templateDecoded').text();
// _(templateRaw)
const $tbodyDecoded = $('#tableDecoded > div.tbody');

const PACKET_START = 0x7B;
const PACKET_STOP = 0x7D;
const PACKET_ESC = 0x7C;
const PACKET_XOR = 0x20;

const SERIAL_TX_STATUS = 0x00;
const SERIAL_TX_PACKET = 0x01;
const SERIAL_RX_BTN1 = 0x02;
const SERIAL_RX_BTN2 = 0x03;

const { SerialPort } = require('serialport')
const { PktParser } = require('@serialport/parser-pkt')
let port;
SerialPort.list().then((ports) => {
  let portPath = false;
  _(ports);
  ports.forEach((port) => {
    _(port);
    if (port.path.indexOf('/dev/tty.usbserial') == 0) portPath = port.path;
    else if (port.path.indexOf('/dev/tty.usbmodem') == 0) portPath = port.path;
  });
  if (portPath === false) {
    alert('Serial port not found.');
    return;
  }

  port = new SerialPort({ path: portPath, baudRate: 115200 });

  // Open errors will be emitted as an error event
  port.on('error', (err) => {
    alert(err.message);
  });

  const parser = port.pipe(new PktParser());

  parser.on('data', pkt => {
    _(pkt)
    if (pkt.type == SERIAL_TX_PACKET) {
      // decoder.process(pkt);
      // pkt.trClass = `ID${pkt.id}`;
      // pkt.json = JSON.stringify(pkt)
      // _(pkt)
      decoder.process(pkt);
      pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
      pkt.json = JSON.stringify(pkt)
      addRows(pkt);
    }
  })

});

const decoder = new Decoder();

let autoScroll = true;
let filterActive = false;

/*
{
  let pkt = {
    millis: 1234,
    id: 0x27F,
    rr: 1,
    dl: 1,
    data: [6]
  };
  _(pkt)
  decoder.process(pkt);
  pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
  pkt.json = JSON.stringify(pkt)
  addRows(pkt);
}

{
  let pkt = {
    millis: 2345,
    id: 0x27F,
    rr: 0,
    dl: 2,
    data: [10, 8]
  };
  _(pkt)
  decoder.process(pkt);
  pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
  pkt.json = JSON.stringify(pkt)
  addRows(pkt);
}

{
  let pkt = {
    millis: 3456,
    id: 0x50A,
    rr: 0,
    dl: 8,
    data: [0, 10, 0, 20, 0, 30, 0, 40]
  };
  _(pkt)
  decoder.process(pkt);
  pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
  pkt.json = JSON.stringify(pkt)
  addRows(pkt);
}

{
  let pkt = {
    millis: 4567,
    id: 0x600,
    rr: 0,
    dl: 6,
    data: [72, 69, 76, 76, 79, 13]
  };
  _(pkt)
  decoder.process(pkt);
  pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
  pkt.json = JSON.stringify(pkt)
  addRows(pkt);
}
*/

$('.fillData').on('click', () => {
  let m = 0;
  Object.keys(CAN_IDS).sort().forEach(id => {
    let pkt = {
      millis: m++,
      id: id,
      rr: 0,
      dl: 4,
      data: [1, 2, 3, 4]
    };
    _(pkt)
    decoder.process(pkt);
    pkt.trClass = `CAT${pkt.id.substring(0, 1)}`;
    pkt.json = JSON.stringify(pkt);
    addRows(pkt);
  });
});

$('.btnToggleView').on('click', function (e) {
  if ($(this).data('decode') == '1') { // switching to decoded view
    const scrollTop = $tbodyRaw[0].scrollTop;
    $('#tableRaw').hide();
    $('#tableDecoded').show();
    $tbodyDecoded[0].scrollTop = scrollTop;
    currentViewIsRaw = false;
  } else { // switching to raw view
    const scrollTop = $tbodyDecoded[0].scrollTop;
    $('#tableDecoded').hide();
    $('#tableRaw').show();
    $tbodyRaw[0].scrollTop = scrollTop;
    currentViewIsRaw = true;
  }
});

$('.btnClear').on('click', function (e) {
    $('#tableRaw div.tbody, #tableDecoded div.tbody').empty();
});

$('.filterCB').on('click', function (e) {
  filterActive = this.checked;
  $('.filterCB').prop('checked', this.checked);
  if (filterActive) {
    $('#filterForm').show();
    $('.tbody').css('bottom', 65);
  } else {
    $('#filterForm').hide();
    $('.tbody').css('bottom', 32);
    $('.tr').each((i, tr) => {
      $(tr).show();
    });
    filterMask = 0x000; // Show all
    filterId = 0x000; // Show all
  }
});

$('#filterBtn').on('click', function (e) {
  filterMask = parseInt($('#filterForm input[name="filterMask"]')[0].value, 16);
  filterId = parseInt($('#filterForm input[name="filterId"]')[0].value, 16) & filterMask;
  $('.tr').each((i, tr) => {
    let id = parseInt($(tr).data('pkt').id, 16);
    if ((id & filterMask) == filterId) $(tr).show();
    else $(tr).hide();
  });
});

$('.autoScrollCB').on('click', function (e) {
  autoScroll = this.checked;
  $('.autoScrollCB').prop('checked', this.checked);
});

$('.tbody').on('click', '.resend', function (e) {
  const pkt = $(this).closest('.tr').data('pkt');
  _(pkt);
  selectId.value = pkt.id;
  $('#sendForm input[name="rr"]')[0].checked = pkt.rr;
  $('#sendForm input[name="data"]')[0].value = pkt.dataOut;
});

$('#sendBtn, #b1Btn, #b2Btn').on('click', function (e) {
  if (selectId.value == '') {
    alert('Select a packet ID first.');
    return;
  }
  let data = $('#sendForm input[name="data"]')[0].value.trim();
  if (data === '') {
    data = [];
  } else {
    data = data.split(' ');
    data.forEach((v, i) => {
      if (v.indexOf('0x') == 0) {
        data[i] = parseInt(v, 16);
      } else {
        data[i] = parseInt(v);
      }
    });
  }

  const id = parseInt(selectId.value, 16);
  const rr = $('#sendForm input[name="rr"]')[0].checked ? 1 : 0;
  let txBuf = [], txOutBuf = [];
  txBuf.push(id & 0xFF, id >> 8, rr, data.length, ...data);
  _('txBuf', txBuf);
  txOutBuf.push(PACKET_START);
  if (this.id == 'sendBtn') txOutBuf.push(SERIAL_TX_PACKET);
  else if (this.id == 'b1Btn') txOutBuf.push(SERIAL_RX_BTN1);
  else txOutBuf.push(SERIAL_RX_BTN2);
  txBuf.forEach((c) => {
    if (c == PACKET_START || c == PACKET_STOP || c == PACKET_ESC) {
      txOutBuf.push(PACKET_ESC, c ^ PACKET_XOR);
    } else {
      txOutBuf.push(c);
    }
  });
  txOutBuf.push(PACKET_STOP);
  _('txOutBuf', txOutBuf);
  port.write(txOutBuf, function (err) {
    if (err) {
      _('Error on write: ', err.message);
    } else {
      _('message written');
    }
  });
});

function addRows(pkt) {
  let id = parseInt(pkt.id, 16);
  if ((id & filterMask) != filterId) pkt.hide = true;
  _(pkt);

  let rendered = Mustache.render(templateRaw, pkt);
  $tbodyRaw.append(rendered);
  if (autoScroll) $tbodyRaw[0].scroll({ top: $tbodyRaw[0].scrollHeight });

  rendered = Mustache.render(templateDecoded, pkt);
  $tbodyDecoded.append(rendered);
  if (autoScroll) $tbodyDecoded[0].scroll({ top: $tbodyDecoded[0].scrollHeight });
}

function _(...v) {
  console.log(...v);
}
