class Decoder {
  process(pkt) {
    pkt.seconds = (pkt.millis / 1000).toFixed(2);
    pkt.id = this.int2hex(pkt.id, 3);
    pkt.dataOut = [];
    for (var i = 0; i < pkt.data.length; i++) pkt.dataOut[i] = `0x${this.int2hex(pkt.data[i], 2)}`;
    pkt.dataOut = pkt.dataOut.join(' ');
    pkt.decoded = pkt.data.join(' ');
    if (CAN_IDS[pkt.id]) {
      pkt.type = CAN_IDS[pkt.id].name;
      if (CAN_IDS[pkt.id].func) pkt.decoded = this[CAN_IDS[pkt.id].func](pkt);
    }
  }

  int2hex(i, p) {
    let s = '000000000' + i.toString(16);
    return s.substring(s.length - p).toUpperCase();
  }

  CMD_SPEAK(pkt) {
    _('CMD_SPEAK', pkt);
    if (pkt.dl < 1) return `ERROR: expected 1-2 bytes, rec'd ${pkt.dl}`;
    let group = 'unknown';
    if (pkt.dl == 1) {
      if (pkt.data[0] in TS_GROUPS) group = TS_GROUPS[pkt.data[0]];
      return `Group: ${group}`;
    } else {
      let vol = pkt.data[0];
      if (pkt.data[1] in TS_GROUPS) group = TS_GROUPS[pkt.data[1]];
      return `Group: ${group} Vol: ${vol}`;
    }
  }

  STATUS_RC_VT(pkt) {
    _('STATUS_RC_VT', pkt);
    if (pkt.dl < 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const mainV = ((pkt.data[0] << 8 | pkt.data[1]) / 10).toFixed(1);
    const logicV = ((pkt.data[2] << 8 | pkt.data[3]) / 10).toFixed(1);
    const temp1 = ((pkt.data[4] << 8 | pkt.data[5]) / 10).toFixed(1);
    const temp2 = ((pkt.data[6] << 8 | pkt.data[7]) / 10).toFixed(1);
    return `Main V: ${mainV} Temp 1: ${temp1}°C`;
  }

  SPEAK_DIRECT(pkt) {
    _('STATUS_RC_VT', pkt);
    if (pkt.dl == 0) return `ERROR: expected 1-8 bytes, rec'd ${pkt.dl}`;
    let s = 'Say: ';
    for (var i = 0; i < pkt.dl; i++) {
      if (pkt.data[i] == 13) s = s + ' [say it]';
      else s = s + String.fromCharCode(pkt.data[i]);
    }
    return s;
  }

  CMD_ARM(pkt) {
    _('CMD_ARM', pkt);
    if (pkt.dl == 0) return `ERROR: expected 1+ bytes, rec'd ${pkt.dl}`;
    let s = 'Unknown';
    switch (pkt.data[0]) {
      case 0x00:
        s = 'HALT';
        break;
      case 0x01:
        s = 'SMOOTH_HALT';
        break;
      case 0x02:
        s = 'DO_IT';
        break;
      case 0x03:
        s = 'MOVE_TO_A';
        break;
      case 0x04:
        s = 'MOVE_TO_B';
        break;
      case 0x05:
        s = 'GRIPPER';
        break;
    }
    return s;
  }

  STATUS_BATT(pkt) {
    _('STATUS_BATT', pkt);
    if (pkt.dl < 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    const avgV = ((pkt.data[0] << 8 | pkt.data[1]) / 1000).toFixed(1);
    const avgI = (((pkt.data[2] << 8 | pkt.data[3]) - 32768) / 1000).toFixed(1);
    const maxV = ((pkt.data[4] << 8 | pkt.data[5]) / 1000).toFixed(1);
    const maxI = (((pkt.data[6] << 8 | pkt.data[7]) - 32768) / 1000).toFixed(1);
    return `Avg V: ${avgV} Avg I: ${avgI}`;
  }

  TS_CAN_GRIP(pkt) {
    _('TS_CAN_GRIP', pkt);
    if (pkt.dl < 8) return `ERROR: expected 8 bytes, rec'd ${pkt.dl}`;
    /*
    0 can flags
        6-7 n/a
        5 z 10th bit
        4 z 9th bit
        3 y 10th bit
        2 y 9th bit
        1 x neg
        0 x 9th bit
    1 can x
    2 can y
    3 can z
    4 grip flags
        6-7 n/a
        5 z 10th bit
        4 z 9th bit
        3 y 10th bit
        2 y 9th bit
        1 x neg
        0 x 9th bit
    5 grip x
    6 grip y
    7 grip z

    can_flags = ((cwZ >> 4) & 0b00110000) | ((cwY >> 6) & 0b00001100) | ((cwX >> 8) & 0b00000001) | (0b10 if cwX < 0 else 0b00)
    grip_flags = ((gwZ >> 4) & 0b00110000) | ((gwY >> 6) & 0b00001100) | ((gwX >> 8) & 0b00000001) | (0b10 if gwX < 0 else 0b00)
    */
    const canX = ((pkt.data[0] & 0b00000001) << 8 | pkt.data[1]) * (pkt.data[0] & 0b10 ? -1 : 1);
    const canY = ((pkt.data[0] & 0b00001100) << 6 | pkt.data[2]);
    const canZ = ((pkt.data[0] & 0b00110000) << 4 | pkt.data[3]);
    const gripX = ((pkt.data[4] & 0b00000001) << 8 | pkt.data[5]) * (pkt.data[4] & 0b10 ? -1 : 1);
    const gripY = ((pkt.data[4] & 0b00001100) << 6 | pkt.data[6]);
    const gripZ = ((pkt.data[4] & 0b00110000) << 4 | pkt.data[7]);
    return `Can: ${canX}, ${canY}, ${canZ}  Grip: ${gripX}, ${gripY}, ${gripZ}`;
  }

  CMD_VISION(pkt) {
    _('CMD_VISION', pkt);
    if (pkt.dl < 2) return `ERROR: expected 2 bytes, rec'd ${pkt.dl}`;
    if (pkt.data[0] == 0x01) {
      return `D435 angle: ${pkt.data[1] ? 'Up' : 'Down'}`;
    } else {
      return pkt.decoded;
    }
  }
}