const CAN_IDS = {
    '000': {name: 'ALERT_BUMPER Left'},
    '001': {name: 'ALERT_BUMPER Right'},
    '002': {name: 'ALERT_DROP Left'},
    '003': {name: 'ALERT_DROP Right'},
    '004': {name: 'ALERT_EMERG_STOP'},
    '006': {name: 'ALERT_KILL'},
    '008': {name: 'ALERT_RC_STATUS'},
    '008': {name: 'ALERT_RC_CUR_ALARM'},

    '100': {name: 'TS_BUMP Left'},
    '100': {name: 'TS_BUMP Right'},
    '102': {name: 'TS_DROP Left'},
    '103': {name: 'TS_DROP Right'},
    '104': {name: 'TS_SONIC'},
    '109': {name: 'TS_RC_ENCODERS'},
    '10E': {name: 'TS_VIS_OBST'},
    '10F': {name: 'TS_IMU'},
    '110': {name: 'TS_CAN_GRIP', func: 'TS_CAN_GRIP'},

    '204': {name: 'CMD_SONIC'},
    '208': {name: 'CMD_RC_MOVE'},
    '209': {name: 'CMD_RC_MOVE_DIST'},
    '20A': {name: 'CMD_RC_PARAMS'},
    '20B': {name: 'CMD_RC'},
    '20C': {name: 'CMD_JOYSTICK'},
    '20D': {name: 'CMD_ARM', func: 'CMD_ARM'},
    '20E': {name: 'CMD_VISION', func: 'CMD_VISION'},
    '20F': {name: 'CMD_IMU'},
    '27F': {name: 'CMD_SPEAK', func: 'CMD_SPEAK'},
    '280': {name: 'CMD_RTC'},
    '2FE': {name: 'CMD_SUPER'},
    '2FF': {name: 'CMD_HALT'},

    '300': {name: 'RC_DIRECT'},

    '400': {name: 'CAN_ARM'},
    '400': {name: 'CAN_ARM_STOP'},
    '401': {name: 'CAN_ARM_STOP_DECEL'},
    '402': {name: 'CAN_ARM_LIMP'},
    '404': {name: 'CAN_ARM_INIT'},
    '405': {name: 'CAN_ARM_PARK'},
    '406': {name: 'CAN_ARM_UNPARK'},
    '407': {name: 'CAN_ARM_EXTEND'},
    '408': {name: 'CAN_ARM_DANCE'},
    '409': {name: 'CAN_ARM_HOME_R'},
    '400': {name: 'CAN_ARM_HOME_P'},
    '40A': {name: 'CAN_ARM_TOOL_READY'},
    '40B': {name: 'CAN_ARM_TOOL_GET'},
    '40C': {name: 'CAN_ARM_TOOL_LOCK'},
    '40D': {name: 'CAN_ARM_TOOL_RETURN'},
    
    '420': {name: 'CAN_ARM_DISABLE'},
    '421': {name: 'CAN_ARM_ENABLE'},
    '422': {name: 'CAN_ARM_STATUS'},
    '432': {name: 'CAN_ARM_MOVETO_A'},
    '433': {name: 'CAN_ARM_MOVETO_B'},
    '434': {name: 'CAN_ARM_SEEK'},
    '435': {name: 'CAN_ARM_JOG'},
    '440': {name: 'CAN_ARM_SETPOS'},
    '450': {name: 'CAN_ARM_LED'},
    '451': {name: 'CAN_ARM_NEO'},

    '460': {name: 'CAN_ARM_GRAB_CAN'},

    '480': {name: 'CAN_GRIP_OPEN'},
    '481': {name: 'CAN_GRIP_CLOSE'},
    '482': {name: 'CAN_GRIP_2FINGER'},
    '483': {name: 'CAN_GRIP_3FINGER'},

    '500': {name: 'STATUS_BUMPER'},
    '502': {name: 'STATUS_DROP'},
    '504': {name: 'STATUS_SONIC'},
    '506': {name: 'STATUS_KILL'},
    '508': {name: 'STATUS_RC_STAT'},
    '509': {name: 'STATUS_RC_ENC'},
    '50A': {name: 'STATUS_RC_VT', func: 'STATUS_RC_VT'},
    '50B': {name: 'STATUS_RC'},
    '50C': {name: 'STATUS_BATT', func: 'STATUS_BATT'},
    '50D': {name: 'STATUS_DRIVING'},
    '50D': {name: 'STATUS_ARM'},

    '600': {name: 'SPEAK_DIRECT', func: 'SPEAK_DIRECT'},
}
